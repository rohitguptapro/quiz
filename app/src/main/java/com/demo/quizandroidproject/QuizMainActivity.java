package com.demo.quizandroidproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;

public class QuizMainActivity extends AppCompatActivity implements View.OnClickListener {

    //Declare timer
    CountDownTimer cTimer = null;
    TextView quesSql, timer, ques;
    Button one, two, three, four, next;
    Integer seqNum = 0;
    boolean isOptionSelected = false;
    QuestionMain quizMain = new QuestionMain();
    int counter = 30;
    String selectedAns = "";
    int optionSelected = 0;
    static int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#E5E5E5"));
        }
        getSupportActionBar().hide();

        quesSql = findViewById(R.id.txvQuesSql);
        timer = findViewById(R.id.txvTimer);
        ques = findViewById(R.id.txvQues);
        one = findViewById(R.id.btnOne);
        one.setOnClickListener(this);
        two = findViewById(R.id.btnTwo);
        two.setOnClickListener(this);
        three = findViewById(R.id.btnThree);
        three.setOnClickListener(this);
        four = findViewById(R.id.btnFour);
        four.setOnClickListener(this);
        next = findViewById(R.id.btnNext);
        next.setOnClickListener(this);

        startTimer();
        updateUI();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOne:
                // do your code
                selectedAns = one.getText().toString();
                isOptionSelected = true;
                selectedAns(one);
                unSelectedAns(two);
                unSelectedAns(three);
                unSelectedAns(four);
                optionSelected = 1;
                break;
            case R.id.btnTwo:
                // do your code
                selectedAns = two.getText().toString();
                isOptionSelected = true;
                selectedAns(two);
                unSelectedAns(one);
                unSelectedAns(three);
                unSelectedAns(four);
                optionSelected = 2;
                break;
            case R.id.btnThree:
                // do your code
                selectedAns = three.getText().toString();
                isOptionSelected = true;
                selectedAns(three);
                unSelectedAns(one);
                unSelectedAns(two);
                unSelectedAns(four);
                optionSelected = 3;
                break;
            case R.id.btnFour:
                // do your code
                selectedAns = four.getText().toString();
                isOptionSelected = true;
                selectedAns(four);
                unSelectedAns(one);
                unSelectedAns(two);
                unSelectedAns(three);
                optionSelected = 4;
                break;
            case R.id.btnNext:
                // do your code
                if (!isOptionSelected) {
                    Toast.makeText(this, "Please select a option!", Toast.LENGTH_LONG).show();
                    //self.dialog(message: "Please select a option!")
                    return;
                }
                String selectedValue = selectedAns;
                boolean isCorrect = quizMain.getAnswer(selectedValue);
                if (isCorrect) {
                    score += 1;
                }
                quizMain.getNextQuestion();
                updateUI();
                cTimer.start();
                resetTimer();
                break;
            default:
                break;
        }
    }

    //start timer function
    void startTimer() {
        cTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (millisUntilFinished > 6000) {
                    timer.setTextColor(Color.parseColor("#000000"));
                } else {
                    timer.setTextColor(Color.parseColor("#FF0000"));
                }
                System.out.println(millisUntilFinished);
                timer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
//                cTimer.start();
//                resetTimer();
//                updateUI();

                quizMain.getNextQuestion();
                updateUI();
                cTimer.start();
                resetTimer();
            }
        };
        cTimer.start();
    }

    public void resetTimer() {
        counter = 30;
        updateCounter();
    }

    public void updateCounter() {
        //example functionality
        if (counter > -1) {
            timer.setText(String.valueOf(counter));
            if (counter > 5) {
                timer.setTextColor(Color.BLUE);
            } else {
                timer.setTextColor(Color.RED);
            }

            if (counter == 0) {
                if (isOptionSelected) {
                    String selectedValue = selectedAns;
                    boolean isCorrect = quizMain.getAnswer(selectedValue);
                    if (isCorrect) {
                        score += 1;
                    }
                }
                quizMain.getNextQuestion();
                updateUI();
                resetTimer();
            }

            counter -= 1;
        }
    }

    public void updateUI() {
        seqNum += 1;
        unselectEveryOption();
        quesSql.setText("Question " + seqNum + " of 5");
        if (quizMain.questionNumber < 10) {
            ques.setText(quizMain.getQuestions());
            String[] optionsList = quizMain.getOptions();
            Collections.shuffle(Arrays.asList(optionsList));

            one.setText(optionsList[0]);
            two.setText(optionsList[1]);
            three.setText(optionsList[2]);
            four.setText(optionsList[3]);

            if (seqNum > 5) {
                counter = 0;
                cancelTimer();
                Intent i = new Intent(QuizMainActivity.this, ResultActivity.class);
                startActivity(i);
                //viewController.score = self.score
            }
        }

    }

    public void unselectEveryOption() {
        isOptionSelected = false;
        unSelectedAns(one);
        unSelectedAns(two);
        unSelectedAns(three);
        unSelectedAns(four);
    }

    public void selectedAns(Button sender) {
        //sender.setBackgroundColor(Color.parseColor("#3592FF"));
        sender.setBackgroundResource(R.drawable.selected_button_background);
    }

    public void unSelectedAns(Button sender) {
        //sender.setBackgroundColor(Color.parseColor("#ffffff"));
        sender.setBackgroundResource(R.drawable.start_button_background);
    }

    //cancel timer
    void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }

}