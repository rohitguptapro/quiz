package com.demo.quizandroidproject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class QuestionMain {

    int questionNumber = 0;
    List<Integer> uniqueNumbers = uniqueRandomNumbers();
    boolean isFirstTime = true;

    ArrayList<Question> questions = new ArrayList<>(Arrays.asList(new Question("What is the highest grossing movie of all time?", "Avatar", "Avengers Endgame", "Bahubali The Conclusion", "Titanic"),
            new Question("Which of these films won the Oscar for Best Picture in 1998?", "Titanic", "Matrix", "The Full Monty", "Good Will Hunting"),
            new Question("How many Harry Potter series are there in total?", "Eight", "Two", "Five", "Six"),
            new Question("Who is the director of Avatar Movie?", "James Cameron", "Christopher Nolan", "Nitesh Tiwari", "S. S. Rajamouli"),
            new Question("Who won 2 Oscars in India?", "AR Rahman", "Alexandre Desplat", "Thomas Newman", "James Horner"),
            new Question("Who won Oscar for Best Actor in 2020?", "Joaquin Phoenix", "Leonardo DiCaprio", "Jonathan Pryce ", "Ryan Reynolds"),
            new Question("In which movie was the iconic DeLorean time machine originally a refrigerator?", "Back To The Future", "Gone With The Wind", "Gravity", "The Dark Knight"),
            new Question("Oscar Best Animated film 2021?", "Soul", "Joker", "The Batman", "Superman"),
            new Question("What is the highest grossing movie of 2018?", "Avengers: Infinity War", "The Lion King", "Frozen", "Joker"),
            new Question("What movie did Leo DiCaprio win an Oscar for?", "The Revenant", "The Wolf of Wall Street", "Interstellar", "Inception")));


    //getter methods
    public boolean getAnswer(String selectedAnswer) {
        String questionAnswer = questions.get(questionNumber).getAnswer();
        System.out.println("questionAnswer:" + questionAnswer);
        if (selectedAnswer.matches(questionAnswer)) {
            return true;
        } else {
            return false;
        }
    }

    public String getQuestions() {
        if (isFirstTime) {
            getNextQuestion();
            isFirstTime = false;
        }
        return questions.get(questionNumber).getQuestion();
    }

    public String[] getOptions() {
        String options[] = new String[4];
        options[0] = questions.get(questionNumber).getOption1();
        options[1] = questions.get(questionNumber).getOption2();
        options[2] = questions.get(questionNumber).getOption3();
        options[3] = questions.get(questionNumber).getAnswer();

        return options;
    }

    public void getNextQuestion() {
        System.out.println("questionNumber:" + questionNumber + "uniqueNumbers:" + uniqueNumbers.toString());
        if (uniqueNumbers.size() > 0) {
            questionNumber = uniqueNumbers.get(uniqueNumbers.size() - 1);
            uniqueNumbers.remove(uniqueNumbers.size() - 1);
        }
        System.out.println("questionNumber:" + questionNumber + "uniqueNumbers:" + uniqueNumbers.toString());
    }

    public List<Integer> generateRandomArray() {
        int arr[] = new int[5];
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int randomInteger = random.nextInt(10);
            System.out.println("pseudo random int in a range : " + randomInteger);
            arr[i] += randomInteger;
        }
        System.out.println("new Array:" + arr.toString());
        List<Integer> intList = new ArrayList<Integer>(arr.length);
        for (int i : arr) {
            intList.add(i);
        }
        uniqueRandomNumbers();
        return intList;
    }

    public List<Integer> uniqueRandomNumbers(){
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> finalList = new ArrayList<Integer>();
        for (int i=0; i<10; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        for (int i=0; i<5; i++) {
            System.out.println("unique random nos:"+list.get(i));
            finalList.add(list.get(i));
        }
        return finalList;
    }

}
