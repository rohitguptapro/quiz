package com.demo.quizandroidproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView scr, msg;
    Button retry;
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#e67d20"));
        }

        score = QuizMainActivity.score;
        scr = findViewById(R.id.txvScore);
        msg = findViewById(R.id.txvMsg);
        retry = findViewById(R.id.btnRetry);

        scr.setText(score + "/5");
        if (score == 0 || score == 1 || score == 2) {
            msg.setText("Please try again!");
            msg.setTextColor(Color.RED);
            retry.setVisibility(View.VISIBLE);
        } else if (score == 3) {
            msg.setText("Good job!");
            msg.setTextColor(Color.parseColor("#FFA500"));
            retry.setVisibility(View.GONE);
        } else if (score == 4) {
            msg.setText("Excellent work!");
            msg.setTextColor(Color.parseColor("#90EE90"));
            retry.setVisibility(View.GONE);
        } else {
            msg.setText("You are a genius!");
            msg.setTextColor(Color.parseColor("#0FFF50"));
            retry.setVisibility(View.GONE);
        }

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ResultActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }
}